﻿using Absraction.Modules.User;
using DAL.Repository.IRepository.User;
using DAL.Repository.RepositoryFactory.User;
using Entity.IModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.RepositoryFacade.User
{
    public class UserRepositoryFacade : UserAbstract
    {
        #region Declaration

        private Lazy<IAddUserRepository> _addUserRepositoryObj = null;

        private Lazy<IUpdateUserRepository> _updateUserRepositoryObj = null;

        private Lazy<IDeleteUserRepository> _deleteUserRepositoryObj = null;

        private Lazy<ISelectUserRepository> _selectUserRepositoryObj = null;

        #endregion

        #region Constructor

        public UserRepositoryFacade()
        {

        }

        #endregion

        #region Private Property (Sub Repository Instance)

        private Lazy<IAddUserRepository> AddUserRepositoryInstance
        {
            get
            {
                return
                       _addUserRepositoryObj
                            ??
                             (_addUserRepositoryObj = new Lazy<IAddUserRepository>(() => 
                             UserRepositoryFactory.ExecuteFactory<IAddUserRepository>
                             (UserRepositoryFactory.RepositoryType.AddUserRepository)));

            }
        }

        private Lazy<IUpdateUserRepository> UpdateUserRepositoryInstance
        {
            get
            {
                return
                    _updateUserRepositoryObj
                        ??
                            (_updateUserRepositoryObj = new Lazy<IUpdateUserRepository>(() =>
                              UserRepositoryFactory.ExecuteFactory<IUpdateUserRepository>
                              (UserRepositoryFactory.RepositoryType.UpdateUserRepository)));
            }
        }

        private Lazy<IDeleteUserRepository> DeleteUserRepositoryInstance
        {
            get
            {
                return
                    _deleteUserRepositoryObj
                        ??
                            (_deleteUserRepositoryObj = new Lazy<IDeleteUserRepository>(() =>
                              UserRepositoryFactory.ExecuteFactory<IDeleteUserRepository>
                              (UserRepositoryFactory.RepositoryType.DeleteUserRepository)));
            }
        }

        private Lazy<ISelectUserRepository> SelectUserRepositoryInstance
        {
            get
            {
                return
                    _selectUserRepositoryObj
                            ??
                            (_selectUserRepositoryObj = new Lazy<ISelectUserRepository>(() =>
                              UserRepositoryFactory.ExecuteFactory<ISelectUserRepository>
                              (UserRepositoryFactory.RepositoryType.SelectUserRepository)));
            }
        }

        #endregion

        #region Public Method

        public override async Task<dynamic> AddUserDataAsync(IUserEntity userEntityObj)
        {
            try
            {
                return await AddUserRepositoryInstance
                    ?.Value
                    ?.AddUserAsync(userEntityObj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override async Task<dynamic> DeleteUserDataAsync(IUserEntity userEntityObj)
        {
            try
            {
                return await DeleteUserRepositoryInstance
                    ?.Value
                    ?.DeleteUserAsync(userEntityObj);
            }
            catch(Exception)
            {
                throw;
            }
        }

        public override async Task<IEnumerable<IUserEntity>> GetAllUserDataAsync()
        {
            try
            {
                return await SelectUserRepositoryInstance
                    ?.Value
                    ?.SelectUserAsync();
            }
            catch(Exception)
            {
                throw;
            }
        }

        public override async Task<dynamic> UpdateUserDataAsync(IUserEntity userEntityObj)
        {
            try
            {
                return await UpdateUserRepositoryInstance
                    ?.Value
                    ?.UpdateUserAsync(userEntityObj);
            }
            catch(Exception)
            {
                throw;
            }
        }

        public override void Dispose()
        {
            _addUserRepositoryObj?.Value?.Dispose();
            _addUserRepositoryObj = null;

            _updateUserRepositoryObj?.Value?.Dispose();
            _updateUserRepositoryObj = null;

            _deleteUserRepositoryObj?.Value?.Dispose();
            _deleteUserRepositoryObj = null;

            _selectUserRepositoryObj?.Value?.Dispose();
            _selectUserRepositoryObj = null;
        }

        #endregion
    }
}
