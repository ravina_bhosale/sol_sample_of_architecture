﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.IModelEntity.User
{
    public interface IUserEntity
    {
        decimal? UserID { get; set; }

        String UserName { get; set; }

        String Password { get; set; }
    }
}
