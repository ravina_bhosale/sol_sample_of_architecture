﻿using Entity.IModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ModelEntity.User
{
    [DataContract]
    public class UserEntity :IUserEntity
    {
        [DataMember(EmitDefaultValue =false)]
        public decimal? UserID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public String UserName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public String Password { get; set; }
    }
}
