﻿using Entity.ModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace APi.Services.User
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserService" in both code and config file together.
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
         RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "AddUserAsync")]
        [ServiceKnownType(typeof(UserEntity))]
        Task<object> AddUserAsync(UserEntity userEntityObj);

        [OperationContract]
        [WebInvoke(Method = "POST",
         RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "UpdateUserAsync")]
        [ServiceKnownType(typeof(UserEntity))]
        Task<object> UpdateUserAsync(UserEntity userEntityObj);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "DeleteUserAsync")]
        [ServiceKnownType(typeof(UserEntity))]
        Task<object> DeleteUserAsync(UserEntity userEntityObj);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "SelectUserAsync")]
        Task<List<UserEntity>> SelectUserAsync();
    }
}
