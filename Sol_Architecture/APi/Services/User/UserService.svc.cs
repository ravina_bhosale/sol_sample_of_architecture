﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Entity.ModelEntity.User;
using System.ServiceModel.Activation;
using BAL.Context.User;
using Entity.IModelEntity.User;

namespace APi.Services.User
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UserService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select UserService.svc or UserService.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class UserService : IUserService
    {
        public async Task<object> AddUserAsync(UserEntity userEntityObj)
        {
            try
            {
                UserContext userContextObj = new UserContext();
                return await userContextObj.AddUserDataAsync(userEntityObj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<object> DeleteUserAsync(UserEntity userEntityObj)
        {
            try
            {
                UserContext userContextObj = new UserContext();
                return await userContextObj.DeleteUserDataAsync(userEntityObj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<UserEntity>> SelectUserAsync()
        {
            try
            {
                    UserContext userContextObj = new UserContext();
                    return((List<IUserEntity>) 
                    await userContextObj.GetAllUserDataAsync()).
                    Cast<UserEntity>()
                   .ToList();
                
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<object> UpdateUserAsync(UserEntity userEntityObj)
        {
            try
            {
                UserContext userContextObj = new UserContext();
                return await userContextObj.UpdateUserDataAsync(userEntityObj);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
