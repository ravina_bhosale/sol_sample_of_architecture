﻿using Entity.IModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.IRepository.User
{
    public interface ISelectUserRepository : IDisposable
    {
        Task<IEnumerable<IUserEntity>> SelectUserAsync();
    }
}
