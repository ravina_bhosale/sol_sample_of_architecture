﻿using DAL.Repository.IRepository.User;
using DAL.Repository.Repository.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.RepositoryFactory.User
{
    public static class UserRepositoryFactory
    {
        #region Enum

        public enum RepositoryType
        {
            AddUserRepository=0,
            UpdateUserRepository=1,
            DeleteUserRepository=2,
            SelectUserRepository=3
        }

        #endregion

        #region Declaration

        private static Dictionary<RepositoryType, dynamic> _dicObj = new Dictionary<RepositoryType, dynamic>();

        #endregion

        #region Constructor

        static UserRepositoryFactory()
        {
            AddRepositoryInstance();
        }

        #endregion

        #region Private Method

        private static void AddRepositoryInstance()
        {
            _dicObj.Add(RepositoryType.AddUserRepository,
            new Lazy<IAddUserRepository>(() =>
            AddUserRepository.CreateInstance.Value));

            _dicObj.Add(RepositoryType.UpdateUserRepository,
            new Lazy<IUpdateUserRepository>(() =>
            UpdateUserRepository.CreateInstance.Value));

            _dicObj.Add(RepositoryType.DeleteUserRepository,
            new Lazy<IDeleteUserRepository>(() =>
            DeleteUserRepository.CreateInstance.Value));

            _dicObj.Add(RepositoryType.SelectUserRepository,
             new Lazy<ISelectUserRepository>(() =>
             SelectUserRepository.CreateInstance.Value));

        }
        #endregion

        #region Public Method

        public static TRepository ExecuteFactory<TRepository>(RepositoryType repositoryTypeObj) where TRepository : class
        {
            return _dicObj[repositoryTypeObj].Value as TRepository;
        }

        #endregion 
    }
}
