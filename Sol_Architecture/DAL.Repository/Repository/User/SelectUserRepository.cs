﻿using DAL.Repository.IRepository.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModelEntity.User;
using DAL.ORM.Interface.IConcrete.User;
using DAL.ORM.ConcreteFactory;
using DAL.ORM.ORD;
using Entity.ModelEntity.User;

namespace DAL.Repository.Repository.User
{
    public class SelectUserRepository : ISelectUserRepository
    {
        #region Declaration

        private Lazy<IUserConcrete> _userConcreteObj = null;

        private static Lazy<SelectUserRepository> _selectUserRepositoryObj = null;

        #endregion

        #region Constructor

        private SelectUserRepository()
        {

        }

        #endregion

        #region Property

        private Lazy<IUserConcrete> UserConcreteInstance
        {
            get
            {
                return
                _userConcreteObj
                    ??
                    (_userConcreteObj = new Lazy<IUserConcrete>(() =>
                      FactoryConcrete.ExecuteFactory<IUserConcrete>(FactoryConcrete.ConcreteType.User)));
            }
        }

        public static Lazy<SelectUserRepository> CreateInstance
        {
            get
            {
                return
                    _selectUserRepositoryObj
                    ??
                    (_selectUserRepositoryObj = new Lazy<SelectUserRepository>(() =>
                      new SelectUserRepository()));
            }
        }

        #endregion

        #region Private Select Query Mapping

        private Func<UserGetResultSet, IUserEntity> SelectAllUserData
        {
            get
            {
                return
                    (leUserResultSet) => new UserEntity()
                    {
                        UserID = leUserResultSet.UserID,
                        UserName = leUserResultSet.UserName,
                        Password = leUserResultSet.Password
                    };
            }
        }

        #endregion

        #region Public Method

        public void Dispose()
        {
            _userConcreteObj?.Value?.Dispose();
            _userConcreteObj = null;

            _selectUserRepositoryObj = null;
        }

        public async Task<IEnumerable<IUserEntity>> SelectUserAsync()
        {
            try
            {
                var getQuery = ((IEnumerable<IUserEntity>)
                        await
                        UserConcreteInstance
                        ?.Value
                        ?.GetDataAsync(
                            "Select",
                            null,
                            this.SelectAllUserData
                            ));

                return getQuery;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
