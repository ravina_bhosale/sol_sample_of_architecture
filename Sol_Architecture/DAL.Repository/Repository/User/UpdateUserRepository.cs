﻿using DAL.Repository.IRepository.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModelEntity.User;
using DAL.ORM.Interface.IConcrete.User;
using DAL.ORM.ConcreteFactory;

namespace DAL.Repository.Repository.User
{
    public class UpdateUserRepository : IUpdateUserRepository
    {
        #region Declaration

        private Lazy<IUserConcrete> _userConcreteObj = null;

        private static Lazy<UpdateUserRepository> _updateUserRepositoryObj = null;

        #endregion

        #region Constructor

        private UpdateUserRepository()
        {

        }

        #endregion

        #region Property

        private Lazy<IUserConcrete> UserConcreteInstance
        {
            get
            {
                return
                _userConcreteObj
                    ??
                    (_userConcreteObj = new Lazy<IUserConcrete>(() =>
                      FactoryConcrete.ExecuteFactory<IUserConcrete>(FactoryConcrete.ConcreteType.User)));
            }
        }

        public static Lazy<UpdateUserRepository> CreateInstance
        {
            get
            {
                return
                    _updateUserRepositoryObj
                    ??
                    (_updateUserRepositoryObj = new Lazy<UpdateUserRepository>(() =>
                      new UpdateUserRepository()));
            }
        }

        #endregion

        #region Public Method

        public async Task<dynamic> UpdateUserAsync(IUserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            try
            {
                await UserConcreteInstance
                    ?.Value
                    ?.SetDataAsync
                    (
                        "Update",
                        userEntityObj,
                        (leStatus, leMessage) =>
                        {
                            status = leStatus;
                            message = leMessage;
                        });

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Dispose()
        {
            _userConcreteObj?.Value?.Dispose();

            _updateUserRepositoryObj = null;
        }
        #endregion
    }
}
