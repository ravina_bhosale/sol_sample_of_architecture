﻿using DAL.Repository.IRepository.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModelEntity.User;
using DAL.ORM.Interface.IConcrete.User;
using DAL.ORM.ConcreteFactory;

namespace DAL.Repository.Repository.User
{
    public class AddUserRepository : IAddUserRepository
    {
        #region Declaration

        private Lazy<IUserConcrete> _userConcreteObj = null;

        private static Lazy<AddUserRepository> _addUserRepositoryObj = null;

        #endregion

        #region Constructor

        private AddUserRepository()
        {

        }

        #endregion

        #region Property

        private Lazy<IUserConcrete> UserConcreteInstance
        {
            get
            {
                return
                _userConcreteObj
                    ??
                    (_userConcreteObj = new Lazy<IUserConcrete>(() =>
                      FactoryConcrete.ExecuteFactory<IUserConcrete>(FactoryConcrete.ConcreteType.User)));
            }
        }
        
        public static Lazy<AddUserRepository> CreateInstance
        {
            get
            {
                return
                    _addUserRepositoryObj
                    ??
                    (_addUserRepositoryObj = new Lazy<AddUserRepository>(() =>
                      new AddUserRepository()));
            }
        }

        #endregion

        #region Public Method

        public async Task<dynamic> AddUserAsync(IUserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            try
            {
                await UserConcreteInstance
                    ?.Value
                    ?.SetDataAsync
                    (
                        "Insert",
                        userEntityObj,
                        (leStatus, leMessage) =>
                        {
                            status = leStatus;
                            message = leMessage;
                        });

                return (status == 1) ? true : false;
            }
            catch(Exception)
            {
                throw;
            }
        }

        public void Dispose()
        {
            _userConcreteObj?.Value?.Dispose();

            _addUserRepositoryObj = null;
        }

        #endregion
    }
}
