﻿using DAL.ORM.Interface.IConcrete.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.ORM.ORD;
using Entity.IModelEntity.User;
using DAL.ORM.Interface.ICommon;

namespace DAL.ORM.Concrete.User
{
    internal class UserConcrete : IUserConcrete
    {
        #region Declaration

        private Lazy<UserDcDataContext> _dbObj = null;

        private static Lazy<UserConcrete> _userConcreteObj = null;

        #endregion

        #region Constructor

        private UserConcrete()
        {

        }
        #endregion

        #region Property

        private Lazy<UserDcDataContext> DbInstance
        {
            get
            {
                return
                    _dbObj
                    ??
                    (_dbObj = new Lazy<UserDcDataContext>(() => new UserDcDataContext()));
            }
        }

        public static Lazy<UserConcrete> CreateInstance
        {
            get
            {
                return
                    _userConcreteObj
                    ??
                    (_userConcreteObj = new Lazy<UserConcrete>(() => new UserConcrete()));
            }
                
        }

        #endregion

        #region Public Method

        public void Dispose()
        {
            _dbObj.Value.Dispose();
            _dbObj = null;

            _userConcreteObj = null;
        }

        public async Task<dynamic> GetDataAsync(string command, IUserEntity entityObj, Func<UserGetResultSet, IUserEntity> selector, Action<int?, string> actionStoredProcOutPara = null)
        {
            int? status = null;
            string message = null;

            try
            {
                return await Task.Run(() =>
                {
                    var getQuery =
                    DbInstance
                    ?.Value
                    ?.uspGetUser
                    (
                        command,
                        entityObj?.UserID ?? 0,
                        entityObj?.UserName ?? null,
                        entityObj?.Password ?? null,
                        ref status,
                        ref message
                        )
                        ?.AsEnumerable()
                        ?.Select(selector)
                        .ToList();


                    return getQuery;
                });
            }
            catch(Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> SetDataAsync(string command, IUserEntity entityObj, Action<int?, string> storedProcOutPara = null)
        {
            int? status=null;
            string message=null;

            try
            {
                return await Task.Run(() =>
                {
                    var setQuery =
                    DbInstance
                    ?.Value
                    ?.uspSetUser
                    (
                        command,
                        entityObj?.UserID ?? 0,
                        entityObj?.UserName ?? null,
                        entityObj?.Password ?? null,
                        ref status,
                        ref message
                     );
                    storedProcOutPara(status, message);

                    return setQuery;

                });
            }

            catch(Exception)
            {
                throw;
            }
        }


        #endregion
    }
}
