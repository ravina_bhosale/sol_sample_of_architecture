﻿using DAL.ORM.Interface.ICommon;
using DAL.ORM.ORD;
using Entity.IModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ORM.Interface.IConcrete.User
{
    public interface IUserConcrete : ISetRepository<IUserEntity>,IGetRepository<IUserEntity,UserGetResultSet>,IDisposable
    {
    }
}
