﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ORM.Interface.ICommon
{
    public interface IGetRepository<TEntity,TLinqEntityResult> where TEntity:class where TLinqEntityResult : class
    {
        Task<dynamic> GetDataAsync(string command, TEntity entityObj, Func<TLinqEntityResult, TEntity> selector, Action<int?, String> actionStoredProcOutPara = null);
    }
}
