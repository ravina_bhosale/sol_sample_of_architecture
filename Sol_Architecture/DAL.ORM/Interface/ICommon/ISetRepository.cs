﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ORM.Interface.ICommon
{
    public interface ISetRepository<TEntity> where TEntity:class
    {
        Task<dynamic> SetDataAsync(string command, TEntity entityObj, Action<int?, string> storedProcOutPara = null);
    }
}
