﻿using DAL.ORM.Concrete.User;
using DAL.ORM.Interface.IConcrete.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ORM.ConcreteFactory
{
    public class FactoryConcrete
    {
        #region Enum
        public enum ConcreteType
        {
            User = 0
        };
        #endregion

        #region Declaration
        private static Dictionary<ConcreteType, dynamic> _dicObj = new Dictionary<ConcreteType, dynamic>();
        #endregion

        #region Constructor
        static FactoryConcrete()
        {
            AddConcreteInstance();
        }
        #endregion

        #region Private Method
        private static void AddConcreteInstance()
        {
            _dicObj.Add(ConcreteType.User, new Lazy<IUserConcrete>(() => UserConcrete.CreateInstance.Value));
        }
        #endregion

        #region Public Method
        public static TConcrete ExecuteFactory<TConcrete>(ConcreteType concreteTypeObj) where TConcrete : class
        {
            return _dicObj[concreteTypeObj].Value as TConcrete;
        }
        #endregion 
    }
}
