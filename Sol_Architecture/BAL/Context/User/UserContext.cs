﻿using DAL.RepositoryFacade.User;
using Entity.IModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Context.User
{
    public class UserContext : UserRepositoryFacade
    {
        #region Constructor
        public UserContext():base()
        {

        }
        #endregion

        #region Public Method

        public override Task<dynamic> AddUserDataAsync(IUserEntity userEntityObj)
        {
            try
            {
                return base.AddUserDataAsync(userEntityObj);
            }
            catch(Exception)
            {
                throw;
            }
        }

        public override Task<dynamic> UpdateUserDataAsync(IUserEntity userEntityObj)
        {
            try
            {
                return base.UpdateUserDataAsync(userEntityObj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override Task<dynamic> DeleteUserDataAsync(IUserEntity userEntityObj)
        {
            try
            {
                return base.DeleteUserDataAsync(userEntityObj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override Task<IEnumerable<IUserEntity>> GetAllUserDataAsync()
        {
            try
            {
                return base.GetAllUserDataAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
