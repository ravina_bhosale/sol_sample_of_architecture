﻿using Entity.IModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Absraction.Modules.User
{
    public abstract class UserAbstract : IDisposable
    {
        public abstract void Dispose();

        public abstract Task<dynamic> AddUserDataAsync(IUserEntity userEntityObj);

        public abstract Task<dynamic> UpdateUserDataAsync(IUserEntity userEntityObj);

        public abstract Task<dynamic> DeleteUserDataAsync(IUserEntity userEntityObj);

        public abstract Task<IEnumerable<IUserEntity>> GetAllUserDataAsync();
    }
}
