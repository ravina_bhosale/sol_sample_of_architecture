﻿CREATE PROCEDURE [dbo].[uspSetUser]

	@Command Varchar(50)=NULL,
	
	@UserId Numeric(18,0)=NULL,
	@UserName Varchar(50)=NULL,
	@Password Varchar(50)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT

	AS
	BEGIN
		DECLARE @ErrorMessage varchar(MAX)

			IF @Command='Insert'
				BEGIN
					BEGIN TRANSACTION
					BEGIN TRY 
						
						INSERT INTO tblLogin
						(
							UserName,
							Password
						)
						VALUES
						(
							@UserName,
							@Password
							
						)


						SET @Status=1
						SET @Message='Insert Succesfully'

						COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 

						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='There is some error'

						RAISERROR(@ErrorMessage,16,1)

					END CATCH

				END

				ELSE IF @Command='Update'
				BEGIN 
					BEGIN TRANSACTION

					BEGIN TRY

					SELECT 
						@UserName=CASE WHEN @UserName IS NULL THEN L.UserName ELSE @UserName END,
						@Password=CASE WHEN @Password IS NULL THEN L.Password ELSE @Password END
						FROM tblLogin as L
							WHERE L.UserID=@UserId

						UPDATE tblLogin
						SET UserName=@UserName,
							Password=@Password
						WHERE UserID=@UserId

						SET @Status=1
						SET @Message='Update Successfully'

						COMMIT TRANSACTION
					 END TRY

					 BEGIN CATCH
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION

						SET @Status=0
						SET @Message='There is some error'

						RAISERROR(@ErrorMessage,16,1)
					 END CATCH
				END

			
			IF @Command='Delete'
			BEGIN
					BEGIN TRANSACTION

			BEGIN TRY
				DELETE FROM tblLogin
						WHERE UserID=@UserId

						SET @Status=1
						SET @Message='Delete Successful'

				COMMIT TRANSACTION
			END TRY

			BEGIN CATCH
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				SET @Status=0
				SET @Message='There is some error'

				RAISERROR(@ErrorMessage,16,1)
			END CATCH
		END
	END

	GO



