﻿CREATE PROCEDURE [dbo].[uspGetUser]

	@Command Varchar(50)=NULL,

	@UserId Numeric(18,0)=NULL,
	@UserName Varchar(50)=NULL,
	@Password Varchar(50)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT

AS
	BEGIN

	DECLARE @ErrorMessage varchar (MAX)

	IF @Command='Select'

		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				
				SELECT 
					L.UserID,
					L.UserName,
					L.Password
					 From tblLogin as L

					SET @Status=1
					SET @Message='Get select data'
					
					COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION
						
				SET @Status=0
				SET @Message='Select Exception'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH
		END

		END
	GO