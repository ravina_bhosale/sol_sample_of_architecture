﻿CREATE TABLE [dbo].[tblLogin] (
    [UserID]   NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [UserName] VARCHAR (50) NULL,
    [Password] VARCHAR (50) NULL,
    CONSTRAINT [PK_tblLogin] PRIMARY KEY CLUSTERED ([UserID] ASC)
);

