﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft;
using System.Net;
using System.IO;

namespace UTP.ModuleTest.User
{
    public class WCFRest
    {
        #region Public Methods
        public static async Task<string> PostMessage<TEntity>(TEntity TEntityObj, String URL)
        {
            try
            {
                return await Task.Run<String>(() =>
                {
               //     DataContractJsonSerializer DCJSObj =
               //new DataContractJsonSerializer(typeof(TEntity));
               //     MemoryStream MSObj = new MemoryStream();
               //     DCJSObj.WriteObject(MSObj, TEntityObj);
               //     string Data =
               //         Encoding.UTF8.GetString(MSObj.ToArray(), 0, (int)MSObj.Length);

                    string Data = Newtonsoft.Json.JsonConvert.SerializeObject(TEntityObj);


                    WebClient WebClientObj = new WebClient();
                    WebClientObj.UseDefaultCredentials = true;
                    WebClientObj.Headers["Content-type"] = "application/json";
                    WebClientObj.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; rv:2.0) Gecko / 20100101 Firefox / 4.0");
                    WebClientObj.Encoding = Encoding.UTF8;

                    string ResponseResult = WebClientObj.UploadString(URL, "POST", Data);
                    //Console.WriteLine("Data Succesfully Stored...");

                    return ResponseResult;
                });
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
