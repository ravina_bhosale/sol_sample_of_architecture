﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Entity.ModelEntity.User;
using BAL.Context.User;

namespace UTP.ModuleTest.User
{
    [TestClass]
    public class UserUnitTest
    {
        #region BAL To Database

        //[TestMethod]
        //public void AddUserTestMethod()
        //{
        //    Task.Run(async() =>
        //    {
        //        var userEntityObj = new UserEntity()
        //        {
        //            UserName = "seeta",
        //            Password = "1111",
        //        };

        //        var result = await new UserContext()?.AddUserDataAsync(userEntityObj);
        //        Assert.IsTrue(result);
        //    }).Wait();

        //[TestMethod]
        //public void UpdateUserTestMethod()
        //{
        //    Task.Run(async () =>
        //    {
        //        var userEntityObj = new UserEntity()
        //        {
        //            UserID=3,
        //            UserName="Geeta",
        //            Password="1122"
        //        };

        //        var result = await new UserContext()?.UpdateUserDataAsync(userEntityObj);
        //        Assert.IsTrue(result);
        //    }).Wait();


        //[TestMethod]
        //public void DeleteUserTestMethod()
        //{
        //    Task.Run(async () =>
        //    {
        //        var userEntityObj = new UserEntity()
        //        {
        //            UserID = 3
        //        };

        //        var result = await new UserContext()?.DeleteUserDataAsync(userEntityObj);
        //        Assert.IsTrue(result);
        //    }).Wait();

        //[TestMethod]
        //public void SelectUserTestMethod()
        //{
        //    Task.Run(async () =>
        //    {
        //        var userEntityObj = new UserEntity();
        //        var result = await new UserContext()?.GetAllUserDataAsync();
        //        Assert.IsNotNull(result);
        //    }).Wait();

        #endregion

        #region Api To Database
        //[TestMethod]
        //public void AddUserApiTestMethod()
        //{
        //    Task.Run(async () =>
        //    {
        //        var data = new UserEntity()
        //        {
        //            UserName="Rani",
        //            Password="rani01"
        //        };

        //        string apiUrl = "http://localhost:50194/Services/User/UserService.svc/AddUserAsync";

        //        var result = await WCFRest.PostMessage<UserEntity>(data, apiUrl);


        //        Assert.IsNotNull(result);

        //    }).Wait();


        //[TestMethod]
        //public void UpdateUserApiTestMethod()
        //{
        //    Task.Run(async () =>
        //    {
        //        var data = new UserEntity()
        //        {
        //            UserID=5,
        //            UserName = "Prajna",
        //            Password = "1020"
        //        };

        //        string apiUrl = "http://localhost:50194/Services/User/UserService.svc/UpdateUserAsync";

        //        var result = await WCFRest.PostMessage<UserEntity>(data, apiUrl);


        //        Assert.IsNotNull(result);

        //    }).Wait();


        [TestMethod]
        public void DeleteUserApiTestMethod()
        {
            Task.Run(async () =>
            {
                var data = new UserEntity()
                {
                    UserID = 5
                };

                string apiUrl = "http://localhost:50194/Services/User/UserService.svc/DeleteUserAsync";

                var result = await WCFRest.PostMessage<UserEntity>(data, apiUrl);


                Assert.IsNotNull(result);

            }).Wait();


            //[TestMethod]
            //public void SelectUserApiTestMethod()
            //{
            //    Task.Run(async () =>
            //    {
            //        var data = new UserEntity();

            //        string apiUrl = "http://localhost:50194/Services/User/UserService.svc/SelectUserAsync";

            //        var result = await WCFRest.PostMessage<UserEntity>(data, apiUrl);


            //        Assert.IsNotNull(result);

            //    }).Wait();

            #endregion
        }
}
}
